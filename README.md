# Kvasir-Binary

## Introduction
This is the official repository for, *"Detecting gastrointestinal abnormalities with 
binary classification of the Kvasir-Capsule data-set"*. It is a 15 HEC bachelor thesis in computer science 
that evaluates the use of binary classification with the Kvasir-Capsule dataset [1]. The premise of the 
thesis is that a low miss rate of potentially lethal abnormalities is of utmost importance for a GI 
classification system. This is not a novel concept within medical classification but has not to date 
been explored for the Kvasir-Capsule dataset. The study's proposition for a solution is to balance the 
dataset to mitigate bias, to use binary classification to improve performance, and to have a certain focus 
on false negatives to combat the miss rate of abnormalities. The experimental work presented here provides 
one of the first investigations into how binary classification can be used with the Kvasir-Capsule dataset 
to create a robust detection system with a low miss rate of GI abnormalities.

## Kvasir-Capsule
The original Kvasir-Capsule dataset is presented in the paper, *“Kvasir-Capsule, a video capsule endoscopy 
dataset,”* [1]. It is the largest labeled dataset of GI abnormalities to date, with 47,238 labeled images. 
The set comes with an official split that in essence is two different sets that are created from different 
videos. This creates the possibility to simulate a real-world scenario where the model is trained on one 
set of video frames and then evaluated on a totally different set of video frames. It is strongly encouraged 
to use the split when working with the set.

Kvasir-Capsule has an official repository which contains some baseline experiments, and it can be found 
here https://github.com/simula/kvasir-capsule. 

The full dataset can be downloaded via: https://datasets.simula.no/kvasir-capsule/ as a zip file.

## Terms of use
All the code contained in this repository is released with full rights to reuse for all purposes except commercial. 
The datasets are released according to the initial license of the Kvasir-Capsule set which states that full rights to reuse the datasets for educational or research purposes is granted. To use the datasets for other purposes such as commercial purposes or in competitions, a written permission is required from Simula. When code or the datasets in this repository is reused a reference to the bachelor thesis, *"Detecting gastrointestinal abnormalities with binary classification of the Kvasir-Capsule data-set"*, is required in the documentation.

## Contact
For any questions about the repository or the bachelor thesis contact hollstensson@protonmail.com.

#### Citation

[1] &emsp;	P. H. Smedsrud et al., “Kvasir-Capsule, a video capsule endoscopy dataset,” *Scientific Data*, 
vol. 8, no. 1, May 2021, [doi: 10.1038/s41597-021-00920-z.](https://doi.org/10.1038/s41597-020-00622-y)
