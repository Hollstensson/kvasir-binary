# Datasets
All the datasets presented here is based on the Kvasir-Capsule dataset [1]. It is the largest labeled dataset of GI abnormalities to date, with 47,238 labeled images.
The set comes with an official split that in essence is two different sets that are created from different
videos. This creates the possibility to simulate a real-world scenario where the model is trained on one
set of video frames and then evaluated on a totally different set of video frames. It is strongly encouraged
to use the split when working with the set. The only dataset in this repository that does not use the official
split is Kvasir-Binary. This set will not generalize well to other data and should thus not be used for 
other purposes than comparison.

### Split_0-Binary

Created based on the official split for Kvasir-Capsule [1]. The normal class 
consists of all the images from the class “normal clean mucosa” from the 
official split 0. The not_normal class consists of all the other images from the
official split 0.

The total amount of images per class is:

    • normal = 15 853 images 
    • not_normal = 7 208 images

### Split_1-Binary

Created based on the official split for Kvasir-Capsule [1]. The normal class 
consists of all the images from the class “normal clean mucosa” from the 
official split 1. The not_normal class consists of all the other images from the 
official split 1.

The total amount of images per class is:

    • normal = 18 485 images 
    • not_normal = 5 607 images

### Split_0-Binary-Balanced

Created based on the official split for Kvasir-Capsule [1]. The normal class 
consists of every third image from the class “normal clean mucosa” from the 
official split 0. The not_normal class consists of all the other images from 
official split 0.

The total amount of images per class is:

    • normal = 5 285 images 
    • not_normal = 7 208 images

### Split_1-Binary-Balanced

Created based on the official split for Kvasir-Capsule [1]. The normal class 
consists of every fourth image from the class “normal clean mucosa” from the 
official split 0. The not_normal class consists of all the other images from 
official split 0.

The total amount of images per class is:

    • normal = 4 622 images 
    • not_normal = 5 607 images

### Split_0-Binary-Augmented

Based on the Split_0-Binary-Balanced dataset with data augmentation by 
ImageDataGenerator with the following parameters:

ImageDataGenerator(<br>
&emsp;rescale=1./255,<br>
&emsp;rotation_range=90,<br>
&emsp;zoom_range=0.2,<br>
&emsp;horizontal_flip=True,<br>
&emsp;fill_mode="nearest")

The augmentation was made four times for every image in the normal class and 
three times for every image in the not_normal class.

The total amount of images per class is:

    • normal = 21 140 images 
    • not_normal = 21 624 images

### Split_1-Binary-Augmented

Based on the Split_1-Binary-Balanced dataset with data augmentation by 
ImageDataGenerator with the following parameters:

ImageDataGenerator(<br>
&emsp;rescale=1./255,<br>
&emsp;rotation_range=90,<br>
&emsp;zoom_range=0.2,<br>
&emsp;horizontal_flip=True,<br>
&emsp;fill_mode="nearest")

The augmentation was made five times for every image in the normal class and 
four times for every image in the not_normal class.

The total amount of images per class is:

    • normal = 23 110 images 
    • not_normal = 22 428 images

### Split_0-Binary-Balanced-and-Augmented

A combination of both the Split_0-Binary-Augmented set and the 
Split_0-Binary-Balanced set.

The total amount of images per class is:

    • normal = 26 425 images 
    • not_normal = 28 832 images

### Split_1-Binary-Balanced-and-Augmented

A combination of both the Split_1-Binary-Augmented set and the 
Split_1-Binary-Balanced set.

The total amount of images per class is:

    • normal = 27 732 images 
    • not_normal = 28 035 images

### Kvasir-Binary

**This set does not use the official split, and therefore it will not generalize well to other data and should thus 
not be used for other purposes than comparison.**

This set is based on the Kvasir-Capsule dataset [1]. It contains all the images 
from the full set. The normal class consists of all the images from the class 
“normal clean mucosa” and the not_normal class consists of all other images.

The total amount of images per class is:

    • normal = 34 338 images 
    • not_normal = 7 172 images


#### Citation

[1] &emsp;	P. H. Smedsrud et al., “Kvasir-Capsule, a video capsule endoscopy dataset,” *Scientific Data*,
vol. 8, no. 1, May 2021, [doi: 10.1038/s41597-021-00920-z.](https://doi.org/10.1038/s41597-020-00622-y)
